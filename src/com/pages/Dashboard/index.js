import React, { useState } from 'react';
import { Input } from '../../ui/Input';
import { Button } from '../../ui/Button';
import { Table } from '../../widgets/Table';
import { PublicTemplate } from '../../templates/PublicTemplate';
import { useSelector, useDispatch } from 'react-redux';
import { addElement, deleteElement, editElement } from '../../../services/redux/ducks';
import './style.css';

export const Dashboard = () => {

    const contacts = useSelector(state => state.contactReducer.contacts);
    const dispatch = useDispatch();

    const [contactData, setContactData] = useState({
        name: "",
        email: '',
        phone: ""
    })
    const [activeEdit, setActiveEdit] = useState(false);
    const [edittingElement, setEdittingElement] = useState({});

    const addContact = (e) => {
        setContactData({
            ...contactData,
            [e.target.name]: e.target.value,
        });
    }

    const change = (e) => {
        setEdittingElement({
            ...edittingElement,
            [e.target.name]: e.target.value,
        });
    }

    let newContact = {
        id: contacts.length > 0 ? contacts[contacts.length - 1].id + 1 : 0,
        name: contactData.name,
        email: contactData.email,
        phone: contactData.phone
    }

    function deleteContact(id) {
        dispatch(deleteElement(id));
    }

    function editContact(contact) {
        setEdittingElement(contact);
        setActiveEdit(true);
    }

    function saveContact(contact) {
        dispatch(editElement(contact))
        setActiveEdit(false);
    }

    return (
        <PublicTemplate>
            <div className="home-page" >
                <div className="container">
                    <Input inputId="name" type="text" value={contactData.name} name="name" onChange={addContact} />
                    <Input inputId="email" type="email" value={contactData.email} name="email" onChange={addContact} />
                    <Input inputId="phone" type="text" value={contactData.phone} name="phone" onChange={addContact} />
                    <Button text="Save" buttonFunction={() => {
                        dispatch(addElement(newContact)); setContactData({
                            name: "",
                            email: '',
                            phone: ""
                        })
                    }} />
                </div>
                <Table items={contacts}
                    deleteContact={deleteContact}
                    editContact={editContact}
                    activeEdit={activeEdit}
                    edittingElement={edittingElement}
                    cancel={() => setActiveEdit(false)}
                    change={change}
                    saveContact={saveContact} />
            </div >
        </PublicTemplate>
    )
}