import React from 'react';
import './style.css';

export const PublicTemplate = ({ children }) => {
    return (
        <div className="public-template">
            <div>{children}</div>
        </div>
    )
}
