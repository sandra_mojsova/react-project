import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

export const Button = ({ text, buttonFunction }) => {
    return (
        <div className="Button">
            <button onClick={buttonFunction}>{text}</button>
        </div>
    )
}

Button.propTypes = {
    buttonFunction: PropTypes.func,
    text: PropTypes.string
}