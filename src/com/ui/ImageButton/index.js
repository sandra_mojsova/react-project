import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

export const ImageButton = ({ src, onClick, className }) => {
    return (
        <div className="image-button">
            <img src={src} onClick={onClick} className={className} alt="" />
        </div>
    )
}

ImageButton.propTypes = {
    src: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string
}
