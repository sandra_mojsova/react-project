import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

export const Input = ({ type, inputId, value, onChange, name }) => {
    return (
        <div className="inputs">
            <label htmlFor={inputId}>{inputId}</label>
            <input
                name={name}
                type={type}
                id={inputId}
                value={value}
                onChange={onChange}
            />
        </div>
    )
}

Input.propTypes = {
    type: PropTypes.string,
    inputId: PropTypes.string,
    value: PropTypes.string,
    name: PropTypes.string,
    onChange: PropTypes.func
}