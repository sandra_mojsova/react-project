import React from 'react';
import './style.css';
import icon_close from '../../../assets/images/icon_close.svg';
import icon_check from '../../../assets/images/icon_check.svg';
import icon_edit from '../../../assets/images/icon_edit.svg';
import { Input } from '../../ui/Input';
import { ImageButton } from '../../ui/ImageButton';

export const Table = ({ items, deleteContact, editContact, activeEdit, edittingElement, cancel, change, saveContact }) => {
    return (
        items.length > 0 ? <table>
            <tbody>
                {
                    items.map((item) => {
                        return (
                            <tr key={item.id} className="tr-row">
                                {activeEdit && item.id === edittingElement.id ? <>
                                    <td><Input inputId="name" type="text" value={edittingElement.name} name="name" onChange={change} /></td>
                                    <td><Input inputId="email" type="text" value={edittingElement.email} name="email" onChange={change} /></td>
                                    <td><Input inputId="phone" type="text" value={edittingElement.phone} name="phone" onChange={change} /></td>
                                    <td className="table-buttons">
                                        <ImageButton src={icon_check} className="image-button" onClick={() => { saveContact(edittingElement) }} style={{ background: "green" }} />
                                        <ImageButton src={icon_close} className="image-button" onClick={cancel} alt="" />
                                    </td>
                                </> : <>
                                    <td>{item.name}</td>
                                    <td>{item.email}</td>
                                    <td>{item.phone}</td>
                                    <td className="table-buttons">
                                        <ImageButton src={icon_edit} className="image-button" onClick={() => { editContact(item) }} alt="" />
                                        <ImageButton src={icon_close} className="image-button" onClick={() => { deleteContact(item.id) }} alt="" />
                                    </td>
                                </>}
                            </tr>
                        )
                    })
                }
            </tbody>
        </table > : null
    )
}