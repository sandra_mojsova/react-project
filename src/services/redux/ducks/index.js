const ADD = "ADD";
const DELETE = "DELETE";
const EDIT = "EDIT";

const initialState = {
    contacts: []
}

export const addElement = (element) => {
    return {
        type: ADD,
        payload: element
    }
}

export const deleteElement = (elementId) => {
    return {
        type: DELETE,
        payload: elementId
    }
}

export const editElement = (element) => {
    return {
        type: EDIT,
        payload: element
    }
}

export default function contactReducer(state = initialState, action) {
    switch (action.type) {
        case ADD:
            return {
                ...state,
                contacts: [...state.contacts, action.payload]
            }
        case DELETE:
            return {
                ...state,
                contacts: [...state.contacts.filter(contact => contact.id !== action.payload)]
            }
        case EDIT:
            return {
                ...state,
                contacts: [...state.contacts.map(contact => contact.id === action.payload.id ? action.payload : contact)]
            }

        default: return state
    }
}