import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import contactReducer from './ducks';

const reducers = combineReducers({
    contactReducer
});

const middleware = applyMiddleware(thunk);
export default createStore(reducers, middleware);
